; Show special key code on screen
;
(clear)

(with-state
    (scale 0.4)
    (colour (vector 1 0.5 0))
    (wire-colour (vector 0.5 0 1))
    (translate (vector -15 5 0))
    (build-extruded-type "Bitstream-Vera-Sans-Mono.ttf" "keys:" 1))

(define (build-key-text)
    (with-state
        (hint-unlit)
        (scale 0.15)
        (build-type "Bitstream-Vera-Sans-Mono.ttf" "keys")))

(translate (vector -5 0 0))
(define key-text (build-key-text))
(translate (vector 0 -1 0))
(define special-text (build-key-text))
(translate (vector 0 -1 0))
(define mods-text (build-key-text))

(define (key->string key)
    (format "~a" (char->integer key)))

(define (update-key-text text prefix keys)
    (with-primitive text
        (when (pdata-exists? "s")
            (pdata-set! "s" 0 (format "~a: ~a" prefix keys)))))

(define (update-key)
    (let ([keys-special (keys-special-down)]
            [keys (map key->string (keys-down))]
            [mods (key-modifiers)])
        (update-key-text key-text "keys" keys)
        (update-key-text special-text "special" keys-special)
        (update-key-text mods-text "mods" mods)
        ))

(every-frame (update-key))
